(ns hwo2014bot.playground
  (:require [hwo2014bot.core :as core]
            [hwo2014bot.physics :as physics]))

(def angles [0 0 0.3776844946 1.0919802949 2.1029761821])
;(def angles [0 0 0.18637081418039575 0.5389610982579365 1.038284109184775])
;(def angles [0 0 0.020339628650753845 0.05883237587230495 0.11337423420341142])
;(def angles [0.37768449455241465 1.0919802948746682 2.102976182136957 3.3721559336317326 4.862595840110168])
;(def angles [0.0 0.0 0.8430464623113125 2.4567063075723006 4.74767420741262])
(physics/calculate-curve-constants angles 7.0)

;(def car-data1 [{:id {:name "Rubber Override", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 4.06276662336, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}
;                {:id {:name "Rubber Override2", :color "green"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 5.3815112908928, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}])
;
;(def car-data2 [{:id {:name "Rubber Override", :color "red"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 5.3815112908928, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}
;                {:id {:name "Rubber Override2", :color "green"}, :angle 0.0, :piecePosition {:pieceIndex 0, :inPieceDistance 6.8738810650749445, :lane {:startLaneIndex 0, :endLaneIndex 0}, :lap 0}}])
;
;(process-car-data car-data1 car-data2)

;; temp data
(def mytrack {:id "usa",
                       :name "USA",
                       :pieces [{:length 100.0}
                                {:length 100.0, :switch true}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:length 100.0}
                                {:length 100.0, :switch true}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:length 100.0}
                                {:length 100.0}
                                {:length 100.0}
                                {:length 100.0}
                                {:length 100.0}
                                {:length 100.0, :switch true}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:length 100.0}
                                {:length 100.0, :switch true}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:radius 200, :angle 22.5}
                                {:length 100.0}
                                {:length 100.0}
                                {:length 100.0}
                                {:length 100.0}],
                       :lanes [{:distanceFromCenter -20, :index 0}
                               {:distanceFromCenter 0, :index 1}
                               {:distanceFromCenter 20, :index 2}],
                       :startingPoint {:position {:x -340.0, :y -96.0},
                                       :angle 90.0}})

(update-in mytrack [:pieces] core/calculate-curve-lengths (:lanes mytrack))
