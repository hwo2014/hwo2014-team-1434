(ns hwo2014bot.physics
  ;; enable this to use the angle calculations
  ;; also uncomment out code below in calculate-curve-constants
  ;;(:require [clojure.core.matrix :as matrix])
  )

;; ############
;; Game Physics
;; ############
;;
;; Throttle Control
;; ================
;;
;; The acceleration is defined by the following:
;;
;; dv
;; -- = a - kv
;; dt
;;
;; Where
;;   a = the acceleration constant
;;   k = drag constant
;;   v = initial velocity (really just magnitude of velocity for simulation)
;;
;; Since all time steps are in full ticks. The formula for acceleration
;; becomes:
;;
;; dv = a - kv
;;
;; Which defines the change in velocity for the upcoming tick. So the new
;; velocity V would be:
;;
;; V = v + a - kv
;;
;; However, we don't want to just know the velocity of the next tick, we
;; would like to know the velocity at a certain point in time given a
;; particular throttle! So we transform the above into a recursive function
;; to calculate velocity V:
;;
;; V(t) = V(t - 1) + a - k * V(t - 1)
;;
;; This is a recurrence relation. Solving for it and substituting initial
;; velocity for V(0) gives us:
;;
;;        a - (a - vk)(1 - k)^t
;; V(t) = ---------------------
;;                  k
;;
;; Where
;;   a = the acceleration constant
;;   v = initial velocity
;;   k = drag constant
;;   t = number of ticks
;;
;; We can see that the limit as t goes to infinity = a / k, so:
;; Maximum velocity = a / k
;;
;; Since displacement is not continuously calculated, we cannot use integration
;; here. Displacement in the simulation is equal to the last displacement plus
;; current speed. That means we have something like:
;;
;; D(t) = D(t - 1) + V(t)
;;
;; Again, solving this recurrence relation and substituting for D(0), we get:
;;
;;        a[(1 - k)^(t + 1) + kt + k - 1] + k[-v(1 - k)^(t + 1) - kv + dk +v]
;; D(t) = -------------------------------------------------------------------
;;                                        k^2
;;
;; Where
;;   a = the acceleration constant
;;   v = initial velocity
;;   d = initial displacement
;;   k = drag constant
;;   t = number of ticks
;;
;; Curve Handling
;; ==============
;;
;; The following information may be incorrect, but was what I could gather
;; based on what I saw with the data collected in the simulation. It's by no
;; means a full examination of what the slip model is, since I was unable to
;; fully reverse engineer as much as I had hoped in the time available to
;; me.
;;
;; Now, if you've observed what happens to the tail end of the simulated car
;; after it comes out of a corner. You may have noticed that the tail swings
;; back and forth fairly consistently, albeit just with less amplitude after
;; each swing. That's because it follows the behavior of a damped simple
;; pendulum.
;;
;; The following describes the changes in the slip angle as the car travels
;; along the curve or straightline.
;;
;; P = Theta'' + A * Theta' + B * Theta
;;
;; A and B are dependant on each other and velocity in the following way
;; (note that I'm mixing in lower cases here too to represent different
;; constants):
;;
;; B = b * velocity
;; A = a - B
;;
;; The constants that we can stow away and utilize in all corners are
;; (lowercase) a and b. Note that P = 0 when going on a straight. Also note
;; that F grows differently on curves with differing radii.
;;
;; Since P needs to be constant for us to solve the equation, we have to
;; ride through enough ticks in a corner at the same speed (to produce the
;; same force). Then in order to figure out how that value relates to the
;; centripetal force for that particular curve, we can run at a different
;; speed to get two more constants, c & d where:
;;
;; F = v^2 / r = P * c + d
;;
;; The growth of P, unfortunately, is not explaned simply by the centripetal
;; force of the curve: v^2 / r.
;;
;; Unfortunately, time is winding down and there's not enough left to figure
;; out how these all actually fully relate to each other and how to calculate
;; them within the races, so the discussion ends here for the time being.

(defn calculate-drag-constant
  "Given v1 and v2 being consecutive velocities during constant acceleration,
  calculates the value of k (drag constant). a is usually the change in speed
  for the very first tick on full throttle."
  [a v1 v2]
  (/ (- v2 v1 a)
     (- v1)))

(defn calculate-acceleration-for-speed
  "Calculates the instantaneous acceleration that will bring the car to the
  specified speed after 1 tick. Of course we can't accelerate that fast, so it
  will take longer if the acceleration calculated is outside the capabilities
  of the car. This value must be used in conjunction with the actual
  acceleration value and allowable throttle ranges."
  [target-speed current-speed k]
  (+ target-speed
     (- current-speed)
     (* k current-speed)))

(defn velocity
  "Calculates the velocity for the given tick and parameters.

         a - (a - vk)(1 - k)^t
  V(t) = ---------------------
                    k

  Where
    a = the acceleration constant
    v = initial velocity
    k = drag constant
    t = number of ticks"
  [k a v t]
  (/ (- a (* (- a (* v k))
             (Math/pow (- 1 k) t)))
     k))

(defn displacement
  "Calculates the displacement for the given tick and parameters.

         a[(1 - k)^(t + 1) + kt + k - 1] + k[-v(1 - k)^(t + 1) - kv + dk +v]
  D(t) = -------------------------------------------------------------------
                                         k^2

  Where
    a = the acceleration constant
    v = initial velocity
    d = initial displacement
    k = drag constant
    t = number of ticks"
  [k a d v t]
  (/ (+ (* a
           (+ (Math/pow (- 1 k) (+ t 1))
              (* k t)
              k
              -1))
        (* k
           (+ (* (- v)
                 (Math/pow (- 1 k) (+ t 1)))
              (- (* k v))
              (* d k)
              v)))
     (* k k)))

(defn calculate-curve-constants
  "Given the last 5 angles, solves for :

  Theta'' = - Theta' * A - Theta * B + P

  Then calculates the constants a + b based on the velocity.

  Returns the values in a map {:a a :b b :p P}. See the above comments for
  how a and b are calculated.

  Note that P is not a constant, but the driving force (of the pendulum).

  The angles provided should contain 5 entries, enough so that 3 second
  derivatives can be calculated. This allows it to work with any combination
  of angles in a turn as long as they are the same radius and velocity, with
  the exception of the first which can be an angle of zero and a different
  velocity."
  [angles velocity]
  (let [first-derivatives  (map - (rest angles) angles)
        second-derivatives (map - (rest first-derivatives) first-derivatives)
        ;matrix-a           (matrix/matrix (map vector
        ;                                       (map - (rest first-derivatives))
        ;                                       (map - (drop 2 angles))
        ;                                       (repeat 3 1)))
        ;matrix-b           (matrix/matrix second-derivatives)
        ;[notA notB P]      (matrix/mmul (matrix/inverse matrix-a) matrix-b)
        ;b                  (/ notB velocity)
        ;a                  (+ notA (* b velocity))
        ]
    ;{:a a :b b :p P}
    ))
