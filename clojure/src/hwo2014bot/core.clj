(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.physics :as physics])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

;; ############################################################################
;; NOTE: This bot is woefully incomplete due to time constraints and the below
;; AI is nonexistent. If you are looking for something more interesting, take
;; a look @ the hwo2014bot.physics namespace for some physics models reverse
;; engineered from the simulation data.
;; ############################################################################

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(def PING {:msgType "ping" :data "ping"})

;; This is the planned command to send on the next tick message. Every message
;; is processed, but commands will only be sent out when we have to, which is
;; in response to the "gameStart" message and all "carPositions" messages
;; containing a gameTick field.
(def planned-command (atom PING))

;; Identifies the current tick. This is useful for debugging purposes. It gets
;; updated whenever the game initializes or a message containing a tick is
;; received.
(def current-tick (atom 0))

;; Holds my car id so I can figure out which one it is from the carPositions
(def my-id (atom nil))

;; Identifies the current speed of the car. This is the current tick's position
;; minus the last tick's position.
(def my-car (atom {:current-speed 0.0}))

;; Holds the speed for which we are trying to target. The throttle is then
;; calculated and set to achieve the target speed.
(def target-speed (atom 10.0))

;; Holds the last throttle that was sent to the server. Will be reset if there
;; was a crash or other event that resets the throttle.
(def last-throttle (atom 0.0))

;; Holds the calculated drag constant. 0.02 was what was seen in the tracks
;; during coding, but we calculate it in the race.
(def drag (atom nil))

;; Holds the calculated acceleration constant. 0.2 was what was seen in the
;; tracks during coding, but we recalculate it in the race.
(def acceleration (atom nil))

;; Holds the slip angle constants
(def angle-physics (atom nil))

;; Keeps track of the state of turbo. Nil indicates no turbo available.
;; Otherwise, it contains the map information from the turbo message.
(def turbo (atom nil))

;; Keeps track of whether or not our car crashed and is temporarily out of the
;; race. This should be reset upon respawning.
(def crashed (atom false))

;; Holds the track information.
(def track (atom nil))

;; Holds the car positions for the previous tick and current tick
(def previous-cars (atom nil))
(def current-cars (atom nil))

(defn get-my-angle
  []
  (get-in @current-cars [@my-id :angle]))

(defn get-my-radius
  []
  (let [idx (get-in @current-cars [@my-id :piecePosition :pieceIndex])
        lane-idx (get-in @current-cars [@my-id :piecePosition :lane :startLaneIndex])
        lane (get-in @track [:lanes lane-idx])
        radius (get-in @track [:pieces idx :radius])
        angle (get-in @track [:pieces idx :angle])]
    ;; compute actual radius based on lane
    (if radius
      ((if (neg? angle)
         + -)
       radius (:distanceFromCenter lane)))))

(defn arc-length
  "Calculates the length of the arc."
  [r angle]
  (* 2 Math/PI r (/ angle 360.0)))

(defn calculate-curve-lengths
  [pieces lanes]
  (vec (for [piece pieces]
         (if (:length piece)
           piece
           (assoc piece :lengths
             (vec (for [lane lanes]
                    (arc-length
                     ((if (neg? (:angle piece))
                        + -)
                      (:radius piece) (:distanceFromCenter lane))
                     (:angle piece)))))))))

(defn reset-command!
  "Resets the next command to a ping response."
  []
  (reset! planned-command PING))

(defn apply-throttle!
  "Sets the next command sent to be a throttle command."
  [throttle]
  (println @current-tick "Setting throttle to: " throttle)
  (reset! planned-command {:msgType "throttle" :data throttle}))

(defn reset-turbo!
  "Clears turbo availability."
  []
  (reset! turbo nil))

(defn turbo-available?
  "Returns whether or not turbo is available."
  []
  @turbo)

(defmulti handle-msg :msgType)

(defmethod handle-msg "join" [msg])

(defmethod handle-msg "yourCar" [msg]
  (reset! my-id (:data msg))
  (println "I am:" (:data msg)))

(defmethod handle-msg "gameInit" [msg]
  (let [track-info (get-in msg [:data :race :track])
        track-info (update-in track-info [:pieces] calculate-curve-lengths (:lanes track-info))]
    (reset! track track-info)))

(defmethod handle-msg "gameStart" [msg]
  (apply-throttle! 1.0)
  (reset-turbo!)
  (reset! previous-cars nil)
  (reset! current-cars nil))

(defmethod handle-msg "spawn" [msg]
  (reset-turbo!)
  (apply-throttle! 1.0))

(defn calculate-throttle-for-speed
  "Calculates the throttle to use in order to achieve the target speed."
  [target-speed]
  (let [full-accel (physics/calculate-acceleration-for-speed target-speed (:current-speed @my-car) @drag)]
    (cond
     (< full-accel 0.0) 0.0
     (> full-accel @acceleration) 1.0
     :else (/ full-accel @acceleration))))

(let [positions (atom clojure.lang.PersistentQueue/EMPTY)]
  (defn calculate-acceleration-physics!
    [position]
    (when (nil? @drag)
      (swap! positions conj position)
      (when (> (count @positions) 3)
        (swap! positions pop))
      (when (= (count @positions) 3)
        (let [[d0 d1 d2]  @positions
              v1          (- d1 d0)
              v2          (- d2 d1)
              drag-result (if (> v1 0)
                            (physics/calculate-drag-constant v1 v1 v2))]
          (when drag-result
            (println @current-tick "Calculated physics: acceleration =" v1 ", drag =" drag-result)
            (reset! acceleration v1)
            (reset! drag drag-result)))))))

(let [angles (atom clojure.lang.PersistentQueue/EMPTY)
      velocities (atom clojure.lang.PersistentQueue/EMPTY)
      radii (atom clojure.lang.PersistentQueue/EMPTY)
      eq1data (atom nil)]
  (defn calculate-angle-physics!
    [angle velocity radius]
    (swap! angles conj angle)
    (swap! velocities conj velocity)
    (swap! radii conj radius)
    (when (> (count @angles) 5)
      (swap! angles pop)
      (swap! velocities pop)
      (swap! radii pop))

    ;; if we have the data we need, then calculate the constants a and b
    (when (and (not @angle-physics)
               (= (count @angles) 5))
      (let [[a0 a1 a2 a3 a4] @angles
            [r0 r1 r2 r3 r4] @radii
            [v0 v1 v2 v3 v4] @velocities]
        (when (and r1 r2 r3 r4
                   (every? (comp not zero?) [a1 a2 a3 a4])
                   (> r1 0)
                   (= r1 r2 r3 r4)
                   (< (Math/abs (- v2 v1)) 0.0000000001)
                   (< (Math/abs (- v3 v2)) 0.0000000001)
                   (< (Math/abs (- v4 v3)) 0.0000000001))
          ;(println "radii:" (vec @radii))
          (reset! angle-physics
                  (physics/calculate-curve-constants @angles v4))
          (println "Calculated angle physics:" @angle-physics)
          ;; save this speed's data for further calculations of other constants
          (let [firstd (- a4 a3)
                secondd (- firstd (- a3 a2))
                notB (* (:b @angle-physics)
                        v4)
                notA (- (:a @angle-physics)
                        notB)
                p (+ secondd
                     (* firstd notA)
                     (* a4 notB))]
            (reset! eq1data {:p p :fvr (/ (* v4 v4) r4)})))))

    ;; We need to keep the velocity constant to calculate the constants when
    ;; in a turn. If we're not in a turn, then just accelerate away!
    (when (not @angle-physics)
      (let [next-throttle (if (and radius
                                   (> radius 0)
                                   (> velocity 0)
                                   (not (zero? angle)))
                            (calculate-throttle-for-speed velocity)
                            (calculate-throttle-for-speed Double/MAX_VALUE))]
        (when (not= next-throttle @last-throttle)
          (apply-throttle! next-throttle))))

    ;; If we have a & b already, then we need to calculate c & d...
    (when (and @angle-physics
               (not (:c @angle-physics)))
      ;; TODO: we need to go at a constant speed different
      ;; from the previous to get constants c & d.
      (when (not= (calculate-throttle-for-speed 1.0) @last-throttle)
        (apply-throttle! (calculate-throttle-for-speed 1.0)))
      (let [[_ _ a2 a3 a4] @angles
            [_ _ _ r3 r4] @radii
            [_ _ _ v3 v4] @velocities]
        (when (and r3 r4
                   (> (Math/abs (- v4 v3)) 0.0000000001))
          (let[firstd (- a4 a3)
               secondd (- firstd (- a3 a2))
               notB (* (:b @angle-physics)
                       v4)
               notA (- (:a @angle-physics)
                       notB)
               p2 (+ secondd
                     (* firstd notA)
                     (* a4 notB))
               fvr2 (/ (* v4 v4) r4)
               p1 (:p @eq1data)
               fvr1 (:fvr @eq1data)
               c (/ (- fvr1 fvr2)
                    (- p1 p2))
               d (- fvr1 (* c p1))]
            ;(println "p1" p1 "fvr1" fvr1 "p2" p2 "fvr2" fvr2)
            (swap! angle-physics assoc :c c :d d)
            (println "Updated angle physics (currently wrong c & d):" @angle-physics)))))))

;; TODO: take into account lane switching
(defn calculate-position-delta
  "Calculate the current velocity of a car given the position information of
  the previous location and the current location."
  [{track-pieces :pieces} {idx1 :pieceIndex pos1 :inPieceDistance lane1 :lane} {idx2 :pieceIndex pos2 :inPieceDistance lane2 :lane}]
  (if (= idx1 idx2)
    (- pos2 pos1)
    (let [piece (track-pieces idx1)]
      (if (:angle piece)
        (+ pos2 (- ((:lengths piece) (:startLaneIndex lane1))
                   pos1))
        (+ pos2 (- (:length piece)
                   pos1))))))

(defn calculate-radius-for-piece
  [pieceIdx]
  (let [idx (mod pieceIdx (count (@track :pieces)))
        lane-idx (get-in @current-cars [@my-id :piecePosition :lane :startLaneIndex])
        lane (get-in @track [:lanes lane-idx])
        radius (get-in @track [:pieces idx :radius])
        angle (get-in @track [:pieces idx :angle])]
    ;; compute actual radius based on lane
    (if radius
      ((if (neg? angle)
         + -)
       radius (:distanceFromCenter lane)))))

;; TODO: really do it for all cars and add in acceleration calculation
(defn process-car-data
  "Calculates for each car the current speed and current acceleration."
  [car-data-1 car-data-2]
  (calculate-position-delta @track
                            (:piecePosition (get car-data-1 @my-id))
                            (:piecePosition (get car-data-2 @my-id))))

;; TODO: update pieceDistance to utilize correct car info
(defmethod handle-msg "carPositions" [msg]
  (if (= @current-tick 1)
    (reset! planned-command {:msgType "switchLane" :data "Right"}))
  ;(println @current-tick msg)
  ;; save car position data
  (reset! previous-cars @current-cars)
  (reset! current-cars (apply hash-map
                              (mapcat (juxt :id identity)
                                      (:data msg))))
  (when @previous-cars
    (swap! my-car assoc :current-speed (process-car-data @previous-cars @current-cars))
    ;(println @current-tick "Current speed:" (:current-speed @my-car))
    )

  ;; TODO: the below could be optimized such that we're not wasting one tick
  ;; in between each stage...
  (cond

   ;; first calculate the acceleration and drag
   (not @drag)
   (calculate-acceleration-physics! (get-in @current-cars [@my-id :piecePosition :inPieceDistance]))

   ;; then figure out the angle physics
   ;;(not (and @angle-physics
   ;;          (:c @angle-physics)))
   ;;(calculate-angle-physics! (get-my-angle) (:current-speed @my-car) (get-my-radius))

   ;; now race!
   ;; TODO: actually put in some AI here to race!
   :else
    (let [next-throttle (let [radii (keep #(calculate-radius-for-piece (+ % (get-in @current-cars [@my-id :piecePosition :pieceIndex])))
                                          [0 1 2])]
                          (if (seq radii)
                            (calculate-throttle-for-speed (Math/sqrt (* (apply min radii) 0.425)))
                            (calculate-throttle-for-speed Double/MAX_VALUE)))]
      (when (not= @last-throttle next-throttle)
        (apply-throttle! next-throttle)))))

(defmethod handle-msg "turboAvailable" [msg])

(defmethod handle-msg "lapFinished" [msg])

(defmethod handle-msg "finish" [msg])

(defmethod handle-msg "gameEnd" [msg])

(defmethod handle-msg "tournamentEnd" [msg])

(defmethod handle-msg :default [msg]
  (println @current-tick "WARNING: Encountered unknown message" msg))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println @current-tick "Joined")
    "gameInit" (println @current-tick "Game initialized" msg)
    "gameStart" (println @current-tick "Race started")
    "crash" (println @current-tick "Someone crashed")
    "lapFinished" (println @current-tick "Completed lap")
    "finish" (println @current-tick "Finished round")
    "turboAvailable" (println @current-tick "Turbo available")
    "gameEnd" (println @current-tick "Race ended")
    "tournamentEnd" (println @current-tick "GAME OVER")
    "error" (println @current-tick (str "ERROR: " (:data msg)))
    :noop))

(defn msg-requires-response?
  "Identifies whether or not the given message requires a response message."
  [{msg-type :msgType, tick? :gameTick}]
  (and (#{"carPositions" "gameStart"} msg-type)
       tick?))

(defn postprocess-command!
  "Does some housekeeping after the command is sent."
  []
  (let [command @planned-command]
    (case (:msgType command)
      "throttle" (reset! last-throttle (:data command))
      :noop))
  (reset-command!))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (when-let [game-tick (:gameTick msg)]
      (reset! current-tick game-tick))
    (log-msg msg)
    (handle-msg msg)
    (when (msg-requires-response? msg)
      (if (not (= PING @planned-command))
        (println @current-tick "Sending Command: " @planned-command))
      (send-message channel (assoc @planned-command :gameTick @current-tick))
      (postprocess-command!))
    (recur channel)))

(defn -main[& [host port botname botkey trackname]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (if trackname
      (send-message channel {:msgType "createRace"
                             :data {:botId {:name botname
                                            :key botkey}
                                    :trackName trackname
                                    :password "PASSWORD"
                                    :carCount 1}})
      (send-message channel {:msgType "join" :data {:name botname :key botkey}}))
    (game-loop channel)))
